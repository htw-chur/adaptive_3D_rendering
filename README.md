# Matlab implementation of user-position aware display of a 3D dataset

## Abstract

We present an approach to use standard desktop PC hardware (monitor and webcam) to display user-position aware projections of 3D data without additional headgear. The user position is detected from webcam images, and the rendered 3D data (i.e. the view) is adjusted to match the corresponding user position, resulting in a quasi virtual reality rendering, albeit without the 3D effect of proper 3D head-gear. The approach has many applications from medical imaging, to construction and CAD, to architecture, to exhibitions, arts and performances.

Depending on the user location, i.e. the detected head position, the data is rendered differently to attribute for the user view angle (zoom) and direction. As the user moves his or her head in front of the monitor, different features of the rendered object become visible. As the user moves closer to the screen, the view angle of the rendered data is decreased, resulting in a zoomed-in version of the rendered object. 

## Principle

![principle](images/principle.png)

Principle of the user-position aware 3D rendering. Depending on the detected user head position, the rendering is adjusted to attribute for the user view direction and field of view (zoom). As the user moves to the left or right, different features of the rendered object become visible. As the user moves closer to the screen, the view angle decreases, resulting in a zoomed-in version of the rendered object (see below). Image parts: Wiki commons.
